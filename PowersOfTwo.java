/* print powers of two. This solution will overflow at 2 to the power of 31 since
 * variable powerOfTwo is an int. Changing that to float or double will increate 
 * the range.
 *
 * requires one argument. 
 */
public class PowersOfTwo {
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        
        int i = 0;
        int powerOfTwo = 1;
        
        while(i <= n) {
            System.out.println("2 to the power of " + i + " is " + powerOfTwo);
            powerOfTwo = 2 * powerOfTwo;
            i += 1;
        }
    }
}